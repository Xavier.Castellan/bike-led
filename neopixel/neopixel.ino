// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            4

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      16

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 100; // delay for half a second

#define NUMLINES 6

unsigned char  pixel []={
                         
                         
                         128,0,0,128, 0,0,0,0, 0,0,0,0, 128,0,0,128,
                         0,128,0,0, 128,0,0,0, 128,0,0,0, 0,128,0,0,
                         0,0,128,0, 0,0,0,128, 0,0,0,128, 0,0,128,0,
                         128,0,0,128, 0,0,0,0, 0,0,0,0, 128,0,0,128,
                          0,128,0,0, 128,0,0,0, 128,0,0,0, 0,128,0,0,
                         0,0,128,0, 0,0,0,128, 0,0,0,128, 0,0,128,0,
                          
                          
                         
                         
                        };

void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.
}


void setPixels(int line)
{
  for(int i=0;i<NUMPIXELS;i++){
      pixels.setPixelColor(i, pixels.Color(pixel[line*16+i],0,0)); // Moderately bright green color.
  }
  pixels.show();
}

void resetPixels()
{
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(0,0,0)); 
  }
  pixels.show(); 
}

void allPixels()
{
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(200,0,0)); 
  }
  pixels.show(); 
}
void display(long on, long off)
{
  allPixels();
  delay(on); 
  resetPixels();
  delay(off);
}
void loop() {
  int i;
  for(i = 0; i < 7; i++){
    display(100,150);
  }
  for(i = 0; i < 6; i++){
    display(300,600);
  }

}